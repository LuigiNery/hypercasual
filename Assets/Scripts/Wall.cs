using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Wall : MonoBehaviour
{
    [Header("Velocidade")]
    [SerializeField]
    private float speed;

    [Header("Posi��o")]
    [SerializeField]
    private float position;
    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        Move();
    }

    public void Move()
    {
        transform.position = new Vector2(transform.position.x + -speed * Time.deltaTime, transform.position.y);

        if (transform.position.x < position)
        {
            PoolManager.ReleaseObject(gameObject);
        }
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.CompareTag("Player"))
        {
            if (Player.wall == true)
            {
                Manager.score += 1;
                PoolManager.ReleaseObject(gameObject);
            }
            else
            {
                Player.dead = true;
            }
            
        }
    }
}
