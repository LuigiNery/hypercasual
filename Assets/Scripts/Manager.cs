using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class Manager : MonoBehaviour
{
    [Header("UI")]
    [SerializeField]
    private GameObject restartButton;
    [SerializeField]
    private Text highScoretxt;
    [SerializeField]
    private Text scoretxt;

    [Header("Objetos")]
    [SerializeField]
    private GameObject[] objetos;


    [Header("Spawn")]
    [SerializeField]
    private Transform spawn;
    [SerializeField]
    private float spawnRate;
    [SerializeField]
    private float nextSpawn;

    [Header("Boost")]
    [SerializeField]
    private float timeBoost;
    [SerializeField]
    private float nextBoost;

    [Header("Score")]
    [SerializeField]
    public static int score;
    [SerializeField]
    private int highScore;


    // Start is called before the first frame update
    void Start()
    {
        highScore = PlayerPrefs.GetInt("HighScore");
        Time.timeScale = 1f;
        score = 0;
        restartButton.SetActive(false);
        nextSpawn = Time.time + spawnRate;
        nextBoost = Time.unscaledTime + timeBoost;
    }

    // Update is called once per frame
    void Update()
    {
        highScoretxt.text = "Maior Pontua��o: " + highScore.ToString();
        scoretxt.text = "Pontua��o: " + score.ToString();

        if (Time.time > nextSpawn)
        {
            Spawn();
        }

        if (Time.unscaledTime > nextBoost)
        {
            Boost();
        }

        if (Player.dead)
        {
            Dead();
        }
    }

    public void Spawn()
    {
        nextSpawn = Time.time + spawnRate;
        int randomObject = Random.Range(0, objetos.Length);
        Instantiate(objetos[randomObject], spawn.position, Quaternion.identity);
    }

    public void Boost()
    {
        nextBoost = Time.unscaledTime + timeBoost;
        Time.timeScale += 0.25f;
    }

    public void Dead()
    {
        if (score > highScore)
        {
            PlayerPrefs.SetInt("HighScore", score);
            highScoretxt.text = score.ToString();
        }

        Time.timeScale = 0;
        restartButton.SetActive(true);
    }

    public void Restart()
    {
        SceneManager.LoadScene("SampleScene");
    }
}
