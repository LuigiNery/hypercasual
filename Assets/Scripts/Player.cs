using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour
{
    [Header("Pulo")]
    public float jump;

    [Header("Ground")]
    [SerializeField]
    private Transform groundCheck;
    [SerializeField]
    private float groundRadius = 0.1f;
    [SerializeField]
    private LayerMask groundLayer;
    [SerializeField]
    private bool isGrounded = false;

    [Header("Abaixar")]
    public bool down = false;

    [Header("Rigidbody")]
    public Rigidbody2D rb;

    public static bool dead;

    // Start is called before the first frame update
    void Start()
    {
        rb = GetComponent<Rigidbody2D>();
<<<<<<< HEAD
        sprite = GetComponent<SpriteRenderer>();
<<<<<<< HEAD
        anim = GetComponent<Animator>();
=======
>>>>>>> parent of b45a5a5 (Commit 4; Mecânicas Novas)
=======
>>>>>>> parent of 2a581ec (Commit5; Assets)
        dead = false;
    }

    // Update is called once per frame
    void Update()
    {
        GroundCheck();
        Down();
<<<<<<< HEAD
        Shield();
        Wall();
<<<<<<< HEAD
        Animation();
=======
>>>>>>> parent of b45a5a5 (Commit 4; Mecânicas Novas)
=======
>>>>>>> parent of 2a581ec (Commit5; Assets)
    }

    public void Jump()
    {
        if(isGrounded)
        {
            rb.AddForce(Vector2.up * jump);
        }
    }

    public void GroundCheck()
    {
        isGrounded = false;

        Collider2D[] colliders = Physics2D.OverlapCircleAll(groundCheck.position, groundRadius, groundLayer);
        if (colliders.Length > 0)
        {
            isGrounded = true;
        }
    }

    public void Down()
    {
<<<<<<< HEAD
<<<<<<< HEAD
        if (down && !dead)
=======
        if (down && isGrounded)
>>>>>>> parent of b45a5a5 (Commit 4; Mecânicas Novas)
=======
        if (down && isGrounded && !dead)
>>>>>>> parent of 2a581ec (Commit5; Assets)
        {
            transform.localScale = new Vector3(0.3f, 0.1f, 0.3f);
        }
        else
        {
            transform.localScale = new Vector3(0.3f, 0.3f, 0.3f);
        }
    }

    public void Pdown()
    {
        down = true;
    }

    public void Pup()
    {
        down = false;
    }

<<<<<<< HEAD
    public void Shield()
    {
        if (shield)
        {
            shieldGameObject.SetActive(true);
        }
        else
        {
            shieldGameObject.SetActive(false);
        }
    }

    public void Wall()
    {
        if (wall)
        {
            sprite.color = new Color(1f, 0f, 0f, 1f);
        }
        else
        {
            sprite.color = new Color(1f, 1f, 1f, 1f);
        }
    }

    public void Dwall()
    {
        wall = true;
    }

    public void Uwall()
    {
        wall = false;
    }

<<<<<<< HEAD
    public void Animation()
    {
        if (isGrounded)
        {
            anim.SetBool("Run", true);
            anim.SetBool("Jump", false);
        }
        else
        {
            anim.SetBool("Run", false);
            anim.SetBool("Jump", true);
        }
    }
=======
>>>>>>> parent of b45a5a5 (Commit 4; Mecânicas Novas)
=======
>>>>>>> parent of 2a581ec (Commit5; Assets)
    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.CompareTag("Enemy"))
        {
            dead = true;
        }
    }
}
